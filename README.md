[![pipeline status](https://gitlab.com/omsai/strom/badges/master/pipeline.svg)](https://gitlab.com/omsai/strom/pipelines)

C++ Bayesian phylogenetics tutorial for https://phylogeny.uconn.edu/software/
