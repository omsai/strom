set -e
builddir=build
mkdir -p $builddir
module purge
module load \
       autoconf/2.69 \
       gcc/5.4.0-alt \
       beagle-lib/3.0.2 \
       ncl-nexus/2.1.18 \
       boost/1.66.0
if [[ "$1" == "reconfig" ]]; then
    # Reconfigure if a magic argument "reconfig" is supplied to this script.
    autoreconf -v --install --force -I m4/
    pushd $builddir >/dev/null
    ../configure --prefix=$HOME/stromapp
else
    make_args="$1"
    pushd $builddir >/dev/null
fi
make --no-print-directory ${make_args}
popd >/dev/null
